module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      require.resolve('babel-plugin-module-resolver'),
      {
        extensions: [".ios.js", ".android.js", ".js",  '.tsx', ".json"],
        "root": ["./src/"],
        alias: {
          // assets: './src/assets',
          // libs: './src/libs',
          theme: './src/themes'
        },
      },
    ],
  ]
};

