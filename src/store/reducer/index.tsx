import {combineReducers} from 'redux';
import ListReducer from './list.reducer';

const Reducer = combineReducers({
    list: ListReducer
});

export default Reducer;