import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
// import AsyncStorage from '@react-native-community/async-storage';
import Reducer from './reducer';

export const store = createStore(Reducer, applyMiddleware(thunk));