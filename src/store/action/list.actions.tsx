import instance from 'utils/api';
import {API_PATH, BASE_URL, TYPE} from 'utils/strings';
import {print} from 'utils/function';
import axios from 'axios';

interface IActions {
    CAR_LIST :string,
    MOVIE_LIST: string,
  } 

interface Dispatch {
    type: IActions['MOVIE_LIST'];
    loading: boolean;
}

export const loading = (payload: Boolean) => ({type: TYPE.load, payload});
export const setMovieList = (payload: any) => ({type: TYPE.movieList, payload});
export const setTotalPage = (payload: number) => ({type: TYPE.movieTPage, payload})
export const setCarList = (payload: any) => ({type: TYPE.carList, payload});


export const getMovieList =(params: any) => async (dispatch: any, getState: any) => {
    try {
        dispatch(loading(true));
        const {movieList} = getState().list;
        params['apikey'] = 'a1b5f9ec';
        const res = await axios.get(BASE_URL, {params});
        let total = res.data.totalResults/10;
        dispatch(loading(false));
        dispatch(setTotalPage(total));
        print(res.data);
        let data = params.page != 1 ? movieList.concat(res.data.Search): res.data.Search;
        dispatch(setMovieList(data));
    } catch (error) {
        dispatch(loading(false));
        throw error;
    }
}

export const getCarList =(data: any) => async (dispatch: any) => {
    try {
        dispatch(loading(true));
        const res = await instance.post('');
        print(res);
        dispatch(loading(false));
        setCarList(res);
    } catch (error) {
        dispatch(loading(false));
        throw error;
    }
}