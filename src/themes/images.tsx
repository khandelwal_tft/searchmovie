const Images = {
    movie: require('../assets/images/movies.webp'),
    car: require('../assets/images/car.png'),
    poster : require('../assets/images/poster.png'),
    search : require('../assets/images/searchIcon.png'),
    clear : require('../assets/images/clear.webp'),
    back : require('../assets/images/backIcon.png'),
}

export default Images;