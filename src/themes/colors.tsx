const Colors = {
  black: '#000',
  bg: '#FCFCFC',
  white: '#FFFFFF',
  themeColor: 'rgb(57,122,188)',
};

export default Colors;
