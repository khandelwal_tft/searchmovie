import React from 'react';
import {View, Text} from 'react-native';
import styles from './styles';

export default function Empty(){
    return (
        <View style={styles.emptyView}>
            <Text style={styles.emptyTxt}>No Data Found.. :(</Text>
        </View>
    )
}