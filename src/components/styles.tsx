import {StyleSheet} from 'react-native';
import { Colors, Fonts, Metrics } from 'themes';

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    emptyView:{
        backgroundColor: Colors.themeColor,
        justifyContent: 'center',
        alignItems: 'center',
        height: Metrics.height * 0.75,
        width: Metrics.width
    },
    emptyTxt: {
        fontSize: Metrics.rfv(24),
        color: Colors.white,
        fontWeight: '400',
    },
    movieCard: {
        flex: 0,
        margin: 6,
        alignItems: 'center',
        padding: 0,
        overflow: 'hidden',
        justifyContent: 'center',
        width: Metrics.width * 0.44,
        borderRadius: 7,
    },
    posterIcon: {
        flex: 1,
        width: '100%',
        height: Metrics.height * 0.22,
        // marginRight: Metrics.rfv(20),
        borderWidth: .3,
    },

    modal: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        padding: Metrics.rfv(10),
        backgroundColor: "rgba(0,0,0,0.6)",
      },
      box: {
        backgroundColor: Colors.white,
        height: Metrics.rfv(100),
        width: Metrics.rfv(100),
        borderRadius: Metrics.rfv(10),
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      },
      title: {
          fontSize: Metrics.rfv(11),
          fontWeight: 'bold',
          textAlign: 'center',
        },
        titleBox: {
            height: Metrics.height * 0.1,
            padding: Metrics.rfv(3),
            justifyContent: 'center',
            alignContent: 'center',
        }
});

export default styles;