import React from 'react';
import {View, Text} from 'react-native';
import FastImage from 'react-native-fast-image';
import { Images} from 'themes';
// import RowItem from './rowItem';
import Shadow from './shadowBox';
import styles from './styles';

export default function MovieCard(props: any){
    return (
        <Shadow style={styles.movieCard}>
        <FastImage
        style={styles.posterIcon}
        resizeMode={FastImage.resizeMode.stretch}
        source={props.Poster != 'N/A' ? {
            uri: props.Poster,
            priority: FastImage.priority.high,
        }: Images.poster}/>
        <View style={styles.titleBox}>
            <Text style={styles.title}>{props.Title}</Text>
            {/* <RowItem title={'Movie Name'} value={props.Title} /> */}
            {/* <RowItem title={'Type'} value={props.Type} /> */}
            {/* <RowItem title={'Released Yame'} value={props.Year} /> */}
            {/* <RowItem title={'IMDB -ID'} value={props.imdbID} /> */}
        </View>
        </Shadow>
    )
}