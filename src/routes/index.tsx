import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {
  createStackNavigator,
  // useHeaderHeight
} from "@react-navigation/stack";
import Movies from '../containers/movies';
import Cars from '../containers/cars';
import { Alert, Image, Pressable, StyleSheet, Text } from 'react-native';
import { Images, Metrics, Colors } from 'themes';

const Tab = createBottomTabNavigator();

const Stack = createStackNavigator();
function Movie() {
  return (
    <Stack.Navigator>
      <Stack.Screen options={{
        headerTitleAlign:'center',
        title:'Search Movies here',
        headerTitleStyle:{color: Colors.themeColor}
      }} name="Movie" component={Movies} />
    </Stack.Navigator>
  );
}
function Car() {
  return (
    <Stack.Navigator>
      <Stack.Screen
      options={{
        headerTitleAlign:'center',
        headerTitleStyle:{color: Colors.themeColor}
      }}
       name="Cars" component={Cars} />
    </Stack.Navigator>
  );
}

function MyTabs() {
  return (
    <Tab.Navigator tabBarOptions={{
      keyboardHidesTabBar:true,
      activeBackgroundColor: Colors.bg,
      activeTintColor: Colors.themeColor,
    }}>
      <Tab.Screen options={{
        tabBarIcon:(props)=> <Image source={Images.movie} style={[s.icon, {backgroundColor: props.color}]} />, //{ focused: boolean, color: string, size: number }
      }} name="Movies" component={Movie} />
      <Tab.Screen name="Cars"
      options={{
        tabBarIcon: (props) => <Image source={Images.car} style={[s.icon,{backgroundColor: props.color}]} />, //{ focused: boolean, color: string, size: number }
      }} component={Car} />
    </Tab.Navigator>
  );
}
const s = StyleSheet.create({
  icon: {
    height: Metrics.rfv(36),
    marginTop: 4,
    width: Metrics.rfv(36),
    borderRadius: 20,
    padding: 15,
    tintColor: Colors.white
  },

})

export default MyTabs;