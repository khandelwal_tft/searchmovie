import Empty from 'components/emptyScr';
import MovieCard from 'components/movieCard';
import React, { useEffect, useState } from 'react';
import {FlatList, Text, View, TouchableOpacity} from 'react-native';
import {RootStateOrAny,  useDispatch, useSelector } from 'react-redux';
import { getMovieList } from 'store/action/list.actions';
import { print } from 'utils/function';
import Search from 'components/searchBar';
import { MOVIE_TYPE } from 'utils/strings';
import styles from './styles';
import { Colors } from 'themes';
import { useNavigation } from '@react-navigation/native';

function MoviesList (props: any) {
    const dispatch = useDispatch();
    const navigation = useNavigation();
    const {movieList, movieTpage} = useSelector((state: RootStateOrAny) => state.list);
    const [page, setPage] = useState(1);
    const [type, setType] = useState('movie');
    const [search, setSearch] = useState('naruto');
    useEffect(()=> {
        fetch();
    },[]);

    async function fetch(tt?: string){
        try {
        let params = {type, s: search, page};
        if(tt) {
            setType(tt);
            params['type'] = tt;
        }
       await dispatch(getMovieList(params));
       print(movieList);
        } catch (error) {
        }
    }

    async function loadMore(){
        let pg = page+1;
        let params = {type, s: search, page: pg};
        setPage(pg);
       await dispatch(getMovieList(params));
    };

    function MovieType(){
       return <View style={styles.typeView}>
        {MOVIE_TYPE.map((item: string, index: number)=> (
            <TouchableOpacity onPress={()=> fetch(item)} style={[styles.typebtn, item== type && {backgroundColor: 'gray'}]} key={index}>
                <Text style={[styles.typeTxt, type == item && {color: "white"}]}>{item}</Text>
            </TouchableOpacity>
        ))}
        </View>
    }

    return (
            <View style={styles.container}>
            <Search 
                searched={(val: string)=> setSearch(val)}
                value={search}
                done={()=> fetch()}
                navigation={navigation}
            />
            {MovieType()}
            <FlatList
                numColumns={2}
                data={movieList}
                extraData={props}
                showsVerticalScrollIndicator={false}
                ListFooterComponent={(movieTpage == page) ? null : <Text onPress={loadMore} style={styles.loadMore}>Load More...</Text>}
                renderItem={({item, index}) =>  <MovieCard key={index} {...item}/>}
                keyExtractor={(item, index) => index.toString()}
                ListEmptyComponent={()=> <Empty/>}
            />
            </View>
    )
}

export default MoviesList;