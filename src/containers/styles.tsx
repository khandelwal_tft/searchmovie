import { StyleSheet } from "react-native";
import {Metrics, Colors} from 'themes';

const styles = StyleSheet.create({
    typebtn: {
        backgroundColor: 'white',
        width: Metrics.width * .22,
        justifyContent: 'center',
        alignItems: 'center',
        height: Metrics.height * 0.038,
        borderRadius: Metrics.height * 0.02,
        borderWidth: 0.3,
        borderColor: 'gray',
        elevation: 1,
    },
    typeView: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginVertical: Metrics.rfv(9),
        marginHorizontal: Metrics.width * 0.025,
    },
    typeTxt: {
        color: Colors.black,
    },
    container: {
        flex:1,
        padding: Metrics.rfv(10),
        backgroundColor: Colors.themeColor,
        paddingBottom: 0,
    },
    loadMore: {
        textAlign: 'center',
        color: Colors.white,
        fontSize: Metrics.rfv(16),
        fontWeight: 'bold',
        marginVertical: Metrics.rfv(15),
    }
});
export default styles;