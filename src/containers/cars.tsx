import React, { useEffect } from 'react';
import Empty from 'components/emptyScr';
import MovieCard from 'components/movieCard';
import styles from 'containers/styles';
import {FlatList, View} from 'react-native';
import { RootStateOrAny, useSelector } from 'react-redux';

function CarList () {
    const {carList} = useSelector((state: RootStateOrAny) => state.list);

    return (
        <FlatList
            style={styles.container}
            data={carList}
            renderItem={({item, index}) =>  <MovieCard key={index} {...item}/>}
            showsVerticalScrollIndicator={false}
            keyExtractor={(item, index) => index.toString()}
            ListEmptyComponent={()=> <Empty/>}
        />
    )
}

export default CarList;