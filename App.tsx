/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import 'react-native-gesture-handler';
import React from 'react';
import {LogBox} from 'react-native';
import { enableScreens } from 'react-native-screens';
import { NavigationContainer } from '@react-navigation/native';
import Tabs from 'routes/index';
import {store} from "./src/store/index";
import { Provider} from 'react-redux';
enableScreens();
LogBox.ignoreAllLogs();
import {Colors} from 'themes';
import {SafeAreaView, StatusBar} from 'react-native';
import Loader from 'components/loader';

declare const global: {HermesInternal: null | {}};

const App = () => {
  return (
    <SafeAreaView style={{flex:1}}>
    <Provider store={store}>
      <StatusBar barStyle="dark-content" backgroundColor={Colors.white} />
      <Loader/>
      <NavigationContainer>
        <Tabs/>
      </NavigationContainer>
    </Provider>
      </SafeAreaView>
  );
};



export default App;
