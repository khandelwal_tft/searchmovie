# React Native Movie Search App Assigment


## Getting started

## Main technologies used

- [React Native](https://github.com/facebook/react-native)

> A framework for building native apps with React.

- [Redux](http://redux.js.org/)

> Redux is a predictable state container for JavaScript apps.

- [react-native-fast-image](https://github.com/DylanVann/react-native-fast-image)

> React Native's Image component handles image caching like browsers for the most part. If the server is returning proper cache control headers for images you'll generally get the sort of built in caching behavior you'd have in a browser. Even so many people have noticed

- [React-Navigation](https://reactnavigation.org/)
> React Native Router based on new React Native Navigation API

## Build and run the app

-  Install React Native as described at [https://facebook.github.io/react-native/docs/getting-started.html#content](https://facebook.github.io/react-native/docs/getting-started.html#content)
-  Clone this repository

- [Install NodeJS](https://nodejs.org/en/) on your computer.

- [Install yarn](https://yarnpkg.com/en/docs/install) on your computer - (optional)
> Yarn is a dependency manager built by facebook and google. It is a more efficient and reliable (thanks to yarn.lock) alternative of npm.

- Install react-native-cli globally on your computer


- Run `npm install` / `yarn install` , all required components will be installed automatically


    ### iOS
      
    1. Run `pod install` from `SearchApp/ios` folder
    2. Start XCode and open generated `SearchApp.xcworkspace`
     
    ### Android
    
    1. Run `npx react-native run-android`
        
    Note: 
    Follow instruction to generate the released android APK
    1. Add `keystore` file to android/app folder 
    2. Open `SearchApp/android` and run command `./gradlew assembleRelease` 


## Package Version

- Package version controlling can be checked out from `package.json`.

Folder Structure
============================


### A typical top-level directory layout

    .
    ├── __tests__               # tests (alternatively `spec` or `tests`)
    ├── android                 # native android folder
    ├── ios                     # native iOS folder
    ├── node_modues             # contain packages
    ├── src                     # Source files 
    ├── app.json                # contain app name
    ├── App.tsx                 # Manage routing
    ├── index.js                # main file
    ├── package.json            # manage version controlling and packages links
    ├── react-native.config.js
    └── README.md


### Source files

    .
    ├── ...
    ├── src
    │   ├── assets
    │   │     ├── images
    │   │     └── fonts
    │   ├── components
    │   │      ├── emptyScr.tsx
    │   │      ├── loader.tsx
    │   │      ├── movieCard.tsx
    │   │      ├── rowItem.tsx
    │   │      ├── searchBar.tsx
    │   │      ├── shadowBox.tsx
    │   │      └── styles.tsx
    │   ├── containers 
    │   │      ├── car.tsx
    │   │      ├── movies.tsx
    │   │      └── styles.tsx
    │   ├── routes
    │   │      └── index.tsx
    │   ├── store
    │   │      ├── action
    │   │      ├── reducer
    │   │      └── styles.tsx   
    │   ├── themes
    │   │      ├── colors.tsx 
    │   │      ├── fonts.tsx
    │   │      ├── images.tsx
    │   │      ├── iphoneX.tsx
    │   │      ├── metrics.tsx
    │   │      └── index.tsx 
    │   ├── utils
    │   │      ├── api.tsx
    │   │      ├── function.tsx
    │   │      └──  string.tsx
    │   └── ...               # etc.
    └── ...




## Troubleshooting
-  It is recommended to run `react-native start` command from root project directory.
-  Run your project from XCode (`Cmd+R`) for iOS, or use `react-native run-android` to run your project on Android.


## Note
-  There is so mobile specific ui so we just tried to copied the web ui
-  This app will support all deivce size

## Have a question

- contact us via `khandelwal.shivam@tftus.com`